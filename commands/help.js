exports.run = (args,client,message) => {
    /*
    Normally i'd run this via an embed but for some people embeds are turned off/broken so in the sake of universal not broken-ness
    i'll just send a boring ole' message
    */

    if(args[0]) {
        let cmd = client.commands.get(args[0])
        if(!cmd) return message.channel.send(`sorry, \`${args[0]}\` is not a command.`)
        message.channel.send(`__**${cmd.config.name}**__\n**help:** \`${cmd.config.help}\`\n**syntax:** \`${cmd.config.syntax}\``)
    } else {
        let str = ""
        client.commands.tap(c => {str += `\n**${c.config.name}** - \`${c.config.help}\` `})
        str += `\n\`do pf!help <command> for syntax regarding that command\``
        message.channel.send(str)
    }
}

exports.config = {
    name:"help",
    help:"you are looking at it now",
    syntax:"pf!help <?command>"
}