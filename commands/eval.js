exports.run = (args,client,message) => {
    if(client.conf.botowner != message.author.id) return
    if(args.join(" ").includes(".token")) return client.iLog(`tried to evaluate string containing ".token"...`,"EVALUATE/ERROR")
    let evaluated = eval(args.join(" "))
    client.iLog(evaluated,"EVALUATE/OUTPUT")
}

exports.config = {
    name:"eval",
    help:"evaluates message as javascript (owner only)",
    syntax:"pf!eval <javascript code>"
}