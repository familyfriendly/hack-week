exports.run = (args,client,message) => {
    //generate embed with the ewmbed generator
   let embed = client.util.genembed({title:`information about ${client.user.username}`,colour:0x0000ff,fields:[
       {name:"owner",value:client.users.get(client.conf.botowner).username,inline:true},
       {name:"pings fixed",value:client.db.get("pings"),inline:true},
       {name:"open source",value:"[click here!](https://gitlab.com/familyfriendly/hack-week)",inline:true},
    ]})
    //send embed
   message.channel.send({embed})
}

exports.config = {
    name:"info",
    help:"displays bot info",
    syntax:"pf!info"
}