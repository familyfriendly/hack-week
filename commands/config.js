exports.run = (args,client,message) => {
    if(!args[0]) return message.channel.send(`e`)
    if(!message.member.hasPermission("ADMINISTRATOR")) return message.channel.send("eeee")
    let data = client.db.get(message.guild.id)
    let savedata = () => {
        client.db.update(message.guild.id,data)
    }
    if(args[0] == "pingmsg") {
        if(!args[1]) return message.channel.send(`no value passed. **variables you can use:**\n[user] - pings user who did ping\n[username] - username of user who pinged\n[rand:|foo,bar,foobar|] - picks random`)
        args = args.splice(1)
        let ping_msg = args.join(" ")
        data.ping.ping_message = ping_msg
        message.channel.send(`ping message set to ${data.ping.ping_message}`)
        return savedata()
    }
    if(args[0] == "pingfix") {
        data.ping.pingfix = !data.ping.pingfix
        message.channel.send(`pinfix set to ${data.ping.pingfix}`)
        return savedata()
    }
    
}



exports.config = {
    name:"config",
    help:"pf!config <setting> <value>",
    syntax:"pf!config pingmsg/pingfix <?value>"
}