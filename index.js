/*
OK time to document this code.
(feel free to laugh at all this spaghetti)
*/

//require discord.js 
const discord = require("discord.js")
//require filesystem
const fs = require("fs")
//create new discord client
var client = new discord.Client()
//get database from wacky-db
const db = require("wacky-db").database
//require config file
client.conf = require("./config/main.json")
//create new wacky-db database
client.db = new db("./data.json")
//require all utils
client.util = require("./util/utils")
//create new collection that will hold all commands
client.commands = new discord.Collection();
//iLog function
//takes message (string) and optional ?type (string)
client.iLog = (message,type) => {
    //if no message is passed return
    if(!message) return
    //check if type is passed, if not replace with INFO
    console.log(`[${type ? type : "INFO"}] / ${message}`)
}
//require event handler and pass client
require('./util/eventLoader')(client);

//read all files in the ./commands directory and add them to the collection commands under client
fs.readdirSync("./commands").map(f => {
    //make sure file is not README.md
    if(f == "README.md") return
    //console log file 
    client.iLog(`loaded file ${f}`,"LOADER")
    //require file
    let fr = require(`./commands/${f}`)
    //add command to the collection
    client.commands.set(fr.config.name,fr)
})

//set activity every 15 minutes
setInterval(() => {
    client.user.setActivity(`${client.db.get("pings")} pings fixed`, {type: 'WATCHING' })
},1000 * 60 * 15)

//login
client.login(client.conf.token)



