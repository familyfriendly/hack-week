# Discord Hack-Week project

![image](https://cdn.discordapp.com/attachments/526792257190494230/592826870077063188/hack_badge_black.png)

## **general idea:**
So i did the genius move of not checking if the problem this bot aims to solve is still a thing, it was not. So the main premise of the bot is that it responds after every @everyone/@here message with a customizable message, in a far away land called the past that would solve a bug that made the notification stay for mobile users but this bug is no more. I still believe you might get some use of it though, maybe like a reminder after every ping or just for old times sake.



## **general info:**
* ## category: **Productivity**
* ## name: **PingFix**
* ## programming language: **JS (nodeJS)**
* ## libraries: **discord.js, wacky-db**
* ## licence: **MIT**
* ## invite: **[click me](https://discordapp.com/api/oauth2/authorize?client_id=592814450084675594&permissions=0&scope=bot)**
>Copyright (c) 2019 Family friendly#0001
>
>Permission is hereby granted, free of charge, to any person obtaining a copy
>of this software and associated documentation files (the "Software"), to deal
>in the Software without restriction, including without limitation the rights
>to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
>copies of the Software, and to permit persons to whom the Software is
>furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all
>copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
>IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
>FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
>AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
>LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
>OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
>SOFTWARE.



## TODO:
Listed in order of importance

### Main bot config
* [x] logging in 
* [x] database
* [x] command handler 
  
### bot related misc
* [x] ping fixed counter
* [x] custom ping fix message
* [x] ping as prefix
* [x] add variables to pingfix message (\$user,\$random) you get the idea

### misc
* [x] logo
* [x] document code fully (hahah like that will ever happen)
* [x] brew coffee

## **how to run:**
Running should be fairly easy! just make sure you have node installed.

### **with git:**
open cmd promt in folder you want to use and run "git clone https://gitlab.com/familyfriendly/hack-week.git"
after all files are downloaded run "npm i" to install all required dependecies

### **without git:**

click download button and do the the things said above - the git part