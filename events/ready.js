module.exports = client => {
    client.iLog(`Client ready!\n->Serving ${client.guilds.size} servers\n-->pings fixed: ${client.db.get("pings")}\n--->did you know ${didyouknow()}`,"READY")
    client.user.setActivity(`${client.db.get("pings")} pings fixed`, {type: 'WATCHING' })
}

let didyouknow = () => {
    let facts = ["Banging your head against a wall for one hour burns 150 calories","In Switzerland it is illegal to own just one guinea pig.","Pteronophobia is the fear of being tickled by feathers.", "Snakes can help predict earthquakes.","The oldest “your mom” joke was discovered on a 3,500 year old Babylonian tablet."]
    return facts[Math.floor(Math.random() * facts.length)]
}
  