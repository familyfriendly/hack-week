module.exports = message => { 
    if(message.channel.type == "dm") return
    if(!message.channel.permissionsFor(message.guild.me).has("SEND_MESSAGES")) return
    let client = message.client
    let pingprefix = {bool:false,long:false}
    if(message.content.startsWith(`<@${client.user.id}>`)) pingprefix = {bool:true,long:false} 
    if(message.content.startsWith(`<@!${client.user.id}>`)) pingprefix = {bool:true,long:true} 
    if(!client.db.exists(message.guild.id)) client.db.write(message.guild.id, {ping:{pingfix:true,ping_message:"ping fix"}})
    let regmatch = message.content.match(/@everyone|@here/gi)
    if(regmatch) {
        let data = client.db.get(message.guild.id)
        if(data.ping.pingfix) {
            let str = data.ping.ping_message
            str = str.replace(/\[user\]|\[username\]|\[rand:\|.+\|\]/gi,(x) => {
                switch(x) {
                    case "[user]":
                    x = message.author
                    break;
                    case "[username]":
                    x = message.author.username
                    break;
                    default:
                            let match = x.match(/(?<=\[rand:\|).+(?=\|\])/gi)
                            console.log(match)
                            if(match) {
                                let arrfm = match[0].split(",")
                                x = arrfm[Math.floor(Math.random() * arrfm.length)]
                            }
                    break;
                }


               return x
            })
            message.channel.send(str)
        }
        try {
            client.db.update("pings",client.db.get("pings")+1)
        } catch(err) {
            client.iLog(err,"ERROR")
        }
    }
    let prefix = pingprefix.bool ? pingprefix.long ? `<@!${client.user.id}>` : `<@${client.user.id}>` :  require("../config/main.json").prefix
    if(!message.content.startsWith(prefix)) return 
    var args = message.content.slice(prefix.length).trim().split(/ +/g);
    var command = args.shift().toLowerCase();
    if(!client.commands.has(command)) return
    let cmd = client.commands.get(command)
    try {
        cmd.run(args,client,message)
    } catch(e) {
        client.iLog(e,`ERROR/${command}`)
    }
}